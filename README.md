**Examen iOS para reclutamiento FACTUM**

El presente repositorio contiene una aplicación de prueba que consiste en lo siguiente:
1. Crear una tabla con diferentes tipos de celdas donde en la primer celda tenga un texfield en el cual se pedirá el nombre y se permitirán únicamente caracteres álfabeticos, la segunda celda pedirá una selfie al usuario al seleccionar la celda se mostrara un popup mostrando la opción de visualizar o retomar/tomar la foto (Puede usar elemetos nativos), la última celda mostrará una descripción sobre “Gráficas” y al seleccionarla mostrará el ejercicio del punto 3.

---
## Patrón de diseño usado: MVP
Este patrón de diseño se usó para desacoplar la vista y el modelo. Toda la comunicación se realiza a través de Presenter.
El modelo es una interfaz responsable de los datos del dominio (que se mostrarán o se ejecutarán en la GUI)
View es responsable de la capa de presentación (GUI)
Presenter es el "puente" entre Model y View. Reacciona a las acciones del usuario realizadas en la Vista, recupera datos del Modelo y los formatea para mostrarlos en la Vista.

## Requisitos para construir el proyecto

Los siguientes son los requisitos necesarios para construir y ejecutar el proyecto:

1. Versión min XCode 12.5

## Pasos para construir el proyecto

1. Descarga o clona el proyecto desde bitbucket 
2. Desde la terminal ubica la carpeta del proyecto
3. Ejecuta **Pod install** para actualizar los pods utilizados en el proyecto.
4. Una vez instalados los pods, abre el proyecto Factum_Exam.xcworkspace
5. Desde el menu, selecciona: Product -> Build

---

## Pasos para ejecutar el proyecto

1. Selecciona el dispositivo deseado para ejecutar el proyecto
2. Click en Play para ejecutar el proyecto.
