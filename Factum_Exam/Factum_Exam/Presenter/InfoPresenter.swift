//
//  InfoPresenter.swift
//  Factum_Exam
//
//  Created by Ligia Montejo on 12/27/21.
//

import Foundation
import UIKit
import FirebaseDatabase

protocol InfoPresenterDelegate: AnyObject {
    func uploadResponse(downloadURL:String)
    func dbChangeValueResponse(color:UIColor)
}

typealias infoPresenterDelegate =  InfoPresenterDelegate & UIViewController


class InfoPresenter {
    
    weak var delegate: infoPresenterDelegate?
    private let database = Database.database().reference()
    
    public func setViewDelegate(delegate: infoPresenterDelegate) {
        self.delegate = delegate
    }
    
    public func firebaseObserve(){
        database.child("FactumConfig").observe(.value, with: { [self] snapshot in
            guard let value = snapshot.value as? [String: Any] else {
                return
            }
            let color = Utils.hexStringToUIColor(hex: value["backgroundColor"] as! String)
            delegate?.dbChangeValueResponse(color: color)
        })
    }
    
    public func sendDataToStorage(fileName:String, image:UIImage) {
        let filePath = "images/\(fileName).jpg"
        PostServiceFireBase.create(for: image, path: filePath) { (downloadURL) in
            guard let downloadURL = downloadURL else {
                print("Download url not found--")
                return
            }
            
            let urlString = downloadURL
            print("image url for download image :: \(urlString)")
           
            self.delegate?.uploadResponse(downloadURL: urlString)
        }
    }
}
