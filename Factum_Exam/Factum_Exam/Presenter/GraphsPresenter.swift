//
//  GraphsPresenter.swift
//  Factum_Exam
//
//  Created by Ligia Montejo on 12/25/21.
//

import UIKit

protocol GraphicsPresenterDelegate: AnyObject {
    func presentGraphics(graphInfo:GraphInfo)
}

typealias GrapPresenterDelegate =  GraphicsPresenterDelegate & UIViewController


class GraphicsPresenter {
    
    weak var delegate: GrapPresenterDelegate?
    
    public func setViewDelegate(delegate: GrapPresenterDelegate) {
        self.delegate = delegate
    }
    
    public func getQuestions() {
        guard let url = URL(string: endPointGraphs ) else { return }
        let task = URLSession.shared.dataTask(with: url) { [weak self] data, _, error in
            guard let data = data, error == nil else { return }
            
            do {
                let graphInfo = try JSONDecoder().decode(GraphInfo.self, from: data)
                
                self?.delegate?.presentGraphics(graphInfo: graphInfo)
            } catch {
                print(error)
            }
        }
        task.resume()
    }

    public func didTapPhoto() {
        
    }
}


