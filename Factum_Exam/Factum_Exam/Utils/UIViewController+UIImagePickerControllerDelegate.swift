//
//  UIViewController+UIImagePickerControllerDelegate.swift
//  Factum_Exam
//
//  Created by Ligia Montejo on 12/25/21.
//

import UIKit

protocol ImagePickerDelegate: AnyObject {
    func gotoController()
}

class ImagePicker {
    var imageHandler: ((UIImage) -> Void)?
    weak var controller: UIViewController?
    weak var delegate: (UIImagePickerControllerDelegate & UINavigationControllerDelegate)?
    weak var controllerDel: ImagePickerDelegate?
    var alert:UIAlertController?
    
    init(controller: UIViewController?) {
        self.controller = controller
        if let delegate = controller as? (UIImagePickerControllerDelegate & UINavigationControllerDelegate) {
            self.delegate = delegate
        }
        alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
    }
    
    func addVisualizeAction() -> ImagePicker {
        alert?.addAction(UIAlertAction(title: "Visualizar/Retomar", style: .default, handler: { _ in
            self.controllerDel?.gotoController()
        }))
        return self
    }
    
    func addTakePicAction() -> ImagePicker {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            alert?.addAction(UIAlertAction(title: "Tomar Selfie", style: .default, handler: { _ in
                self.requestImage(from: .camera)
                
            }))
        }
        return self
    }
    
    func promptUploadImage() {
        alert?.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
        
        if let popoverController = alert?.popoverPresentationController {
            popoverController.sourceView = controller?.view
            popoverController.sourceRect = CGRect(x: (controller?.view.bounds.midX)!, y: (controller?.view.bounds.midY)!, width: 0, height: 0)
          popoverController.permittedArrowDirections = []
        }
        
        controller?.present(alert!, animated: true, completion: nil)
    }
    
    func requestImage(from sourceType: UIImagePickerController.SourceType) {
        let picker = UIImagePickerController()
        picker.delegate = delegate
        picker.allowsEditing = false
        picker.sourceType = sourceType
        controller?.present(picker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
        controller?.dismiss(animated: true, completion: nil)
        if let image = info[.originalImage] as? UIImage {
            imageHandler?(image)
            imageHandler = nil
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        controller?.dismiss(animated: true, completion: nil)
    }
}

