//
//  UIColor+Style.swift
//  Factum_Exam
//
//  Created by Ligia Montejo on 12/22/21.
//

import UIKit

extension UIColor {

    static let textFieldBorder = UIColor(red: 222 / 255.0, green: 229 / 255.0, blue: 238 / 255.0, alpha: 1.0)
}
