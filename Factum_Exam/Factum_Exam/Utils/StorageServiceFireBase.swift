//
//  StorageServiceFireBase.swift
//  Factum_Exam
//
//  Created by Ligia Montejo on 12/27/21.
//

import Foundation
import UIKit
import FirebaseStorage

struct StorageServiceFireBase {

    // provide method for uploading images
    static func uploadImage(_ image: UIImage, at reference: StorageReference, completion: @escaping (URL?) -> Void) {

        guard let imageData = image.jpegData(compressionQuality: 0.1) else {
            return completion(nil)
        }
        
        let metaData = StorageMetadata()
        metaData.contentType = "image/jpg"
        reference.putData(imageData, metadata: metaData, completion: { (metadata, error) in
            if let error = error {
                assertionFailure(error.localizedDescription)
                print("Upload failed :: ",error.localizedDescription)
                return completion(nil)
            }
            
            reference.downloadURL(completion: { url, error in
                guard let url = url, error == nil else {
                    return
                }
                
                completion(url)
            })
        })
    }
}
