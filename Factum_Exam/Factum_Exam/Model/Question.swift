//
//  Question.swift
//  Factum_Exam
//
//  Created by Ligia Montejo on 12/25/21.
//

import Foundation

struct GraphInfo : Codable {
    let colors:[String]
    let questions:[Question]
}

struct Question : Codable {
    let total: Int
    let text: String
    let chartData: [ChartData]
}

struct ChartData : Codable {
    let text: String
    let percetnage : Double
}
