//
//  GraphsViewController.swift
//  Factum_Exam
//
//  Created by Ligia Montejo on 12/25/21.
//

import UIKit
import Charts
import FirebaseDatabase

class GraphsViewController: UIViewController, UITableViewDataSource,UITableViewDelegate, GraphicsPresenterDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    private var questions = [Question]()
    private var pieChartColors = [String]()
    private let presenter = GraphicsPresenter()
    
    var numberOfDownloadsDataEntries = [PieChartDataEntry]()
    private let database = Database.database().reference()

    override func viewDidLoad() {
        super.viewDidLoad()

        //table
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 250
        
        //presenter
        presenter.setViewDelegate(delegate: self)
        presenter.getQuestions()

        database.child("FactumConfig").observe(.value, with: { snapshot in
            guard let value = snapshot.value as? [String: Any] else {
                return
            }
            
            self.view.backgroundColor = Utils.hexStringToUIColor(hex: value["backgroundColor"] as! String)
        })
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    //MARK: TableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return questions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "QuestionCell", for: indexPath) as! QuestionTableViewCell
        cell.question = nil
        let question = questions[indexPath.row]
        cell.colors = pieChartColors
        cell.question = question
        return cell
    }
    
    //Present delegate
    func presentGraphics(graphInfo:GraphInfo) {
        self.pieChartColors = graphInfo.colors
        self.questions = graphInfo.questions
        
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

