//
//  PhotoViewController.swift
//  Factum_Exam
//
//  Created by Ligia Montejo on 12/25/21.
//

import UIKit

protocol PhotoViewControllerDelegate:AnyObject {
    func setImageSelected(image:UIImage)
}

class PhotoViewController: UIViewController {
    
    @IBOutlet weak var userImageView: UIImageView!

    var userImage: UIImage?
    private var imagePicker: ImagePicker?
    weak var delegate:PhotoViewControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()

        let retakeItem = UIBarButtonItem(title: "Retomar", style: .plain, target: self, action: #selector(retakePhoto))
        self.navigationItem.rightBarButtonItem = retakeItem
        
        loadImage()
    }
    
    @objc func retakePhoto() {
        imagePicker = ImagePicker(controller: self)
        imagePicker?.delegate = self
        imagePicker?.requestImage(from: .camera)
    }
    
    func loadImage(){
        userImageView.image = userImage ?? UIImage(named: "no_image")
    }
}

extension PhotoViewController: UINavigationControllerDelegate, UIImagePickerControllerDelegate{

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
        imagePicker?.imagePickerController(picker, didFinishPickingMediaWithInfo: info)
        guard let image = info[.originalImage] as? UIImage else { return }
        self.userImage = image
        self.userImageView.image = image
        self.delegate?.setImageSelected(image: image)
    }
}
