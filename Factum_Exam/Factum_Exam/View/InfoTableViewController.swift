//
//  InfoTableViewController.swift
//  Factum_Exam
//
//  Created by Ligia Montejo on 12/25/21.
//

import UIKit


class InfoTableViewController: UITableViewController, UINavigationControllerDelegate,PhotoViewControllerDelegate {

    @IBOutlet weak var userNameTxt: UITextField!
    
    private var imagePicker: ImagePicker?
    private var userImage: UIImage?
    private let presenter = InfoPresenter()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        userNameTxt.delegate = self
        presenter.delegate = self
        presenter.firebaseObserve()
    }
    
    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return super.tableView(tableView, numberOfRowsInSection: section)
    }
    
    //MARK: - IBActions
    @IBAction func takeImage(_ sender: Any) {
        imagePicker = ImagePicker(controller: self)
        imagePicker?.controllerDel = self
        imagePicker?.delegate = self
        
        imagePicker?.addVisualizeAction()
            .addTakePicAction()
            .promptUploadImage()
    }
    
    @IBAction func sendData(_ sender: Any) {
        guard let img = userImage else { return }
        showLoading()
        presenter.sendDataToStorage(fileName: userNameTxt.text ?? "user1", image: img)
    }
    
    //MARK: - Functions
    func showLoading() {
        let alert = UIAlertController(title: nil, message: "Please wait...", preferredStyle: .alert)

        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.medium
        loadingIndicator.startAnimating();

        alert.view.addSubview(loadingIndicator)
        present(alert, animated: true, completion: nil)
    }
    
    func hideLoading() {
        dismiss(animated: false, completion: nil)
    }
    
    //MARK: - Toast message
    func showToastMessage(message: String) {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Aceptar", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    func setImageSelected(image: UIImage) {
        self.userImage = image
    }
}

extension InfoTableViewController : ImagePickerDelegate {
    func gotoController() {
        let vc = storyboard?.instantiateViewController(withIdentifier: "PhotoViewController") as! PhotoViewController
        vc.userImage = self.userImage
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension InfoTableViewController: UIImagePickerControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
        imagePicker?.imagePickerController(picker, didFinishPickingMediaWithInfo: info)
        if let image = info[.originalImage] as? UIImage {
            self.userImage = image
        }
    }
}

extension InfoTableViewController: UITextFieldDelegate
{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        Utils.validateOnlyAlphabet(string: string)
    }
}

extension InfoTableViewController : InfoPresenterDelegate {
    func dbChangeValueResponse(color: UIColor) {
        self.view.backgroundColor = color
    }
    
    func uploadResponse(downloadURL: String) {
        self.hideLoading()
        self.showToastMessage(message: "Info saved successful")
    }
}
