//
//  BorderTextView.swift
//  Factum_Exam
//
//  Created by Ligia Montejo on 12/22/21.
//

import UIKit

class BorderTextView: UITextView {
    
    private let borderLayer = CAShapeLayer()
    private let maskLayer = CAShapeLayer()
    var corners: UIRectCorner = [.topLeft, .bottomLeft, .topRight, .bottomRight]
    var radii: CGFloat = 18.0
    
    override func awakeFromNib() {
        setup()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        update()
    }
    
    func setup() {
        update()
        borderLayer.fillColor = UIColor.clear.cgColor
        borderLayer.strokeColor = UIColor.textFieldBorder.cgColor
        borderLayer.lineWidth = 2.0
        layer.mask = maskLayer
        layer.addSublayer(borderLayer)
    }
    
    func update() {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radii, height: radii)).cgPath
        maskLayer.path = path
        borderLayer.path = path
    }
}
