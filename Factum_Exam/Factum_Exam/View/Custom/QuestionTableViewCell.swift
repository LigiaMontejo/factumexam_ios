//
//  QuestionTableViewCell.swift
//  Factum_Exam
//
//  Created by Ligia Montejo on 12/22/21.
//

import UIKit
import Charts
import FirebaseDatabase

class QuestionTableViewCell: UITableViewCell {
    
    @IBOutlet weak var questionTitleLbl: UILabel!
    @IBOutlet weak var pieChart: PieChartView!
    
    var colorsToUse = [UIColor]()
    private let database = Database.database().reference()
    
    var question: Question! {
        didSet {
            self.updateChartData()
        }
    }
    
    var colors: [String]! {
        didSet {
            self.updateChartColors()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        pieChart.entryLabelColor = UIColor.clear
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func updateChartData() {
        guard let question = question else { return }
        
        var numberOfDownloadsDataEntries = [PieChartDataEntry]()
        
        questionTitleLbl.text = question.text
        for dataEntry in question.chartData {
            let pieDataEntry = PieChartDataEntry(value: Double(dataEntry.percetnage))
            pieDataEntry.label = "\(dataEntry.text.uppercased()) \(dataEntry.percetnage)%"
            
            numberOfDownloadsDataEntries.append(pieDataEntry)
        }
        var chartDataSet = PieChartDataSet()
        chartDataSet = PieChartDataSet(entries: numberOfDownloadsDataEntries, label: nil)
        let chartData = PieChartData(dataSet: chartDataSet)
        
        chartDataSet.colors = colorsToUse
        
        pieChart.data = chartData
        
        
    }
    
    func updateChartColors() {
        
        colorsToUse.removeAll()
        for color in colors {
            let c = Utils.hexStringToUIColor(hex: color)
            colorsToUse.append(c)
        }
    }
    
}
